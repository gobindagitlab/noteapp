package com.example.noteapp;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.TypedValue;
import android.view.View;
import android.widget.LinearLayout;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.PropertyResourceBundle;

public class MyDividerItemDecoratio extends RecyclerView.ItemDecoration {

    private static  final  int []ATTRS=new int[]{android.R.attr.listDivider};

    public  static final  int HORIZONTAL_LIST=LinearLayoutManager.HORIZONTAL;
    public  static final  int VERTICAL_LIST=LinearLayoutManager.VERTICAL;

    private Drawable mDivider;
    private  int Orentation;
    private Context context;
    private  int margien;
    
    public MyDividerItemDecoratio(Context context, int orientation, int margin) {
        this.context = context;
        this.margien = margin;
        final TypedArray a = context.obtainStyledAttributes(ATTRS);
        mDivider = a.getDrawable(0);
        a.recycle();
        setOrientation(orientation);
    }

    public  void setOrientation(int orentation){
        if (orentation!=HORIZONTAL_LIST && orentation!=VERTICAL_LIST){
            throw  new IllegalArgumentException("Invailed Orientation");
        }
   Orentation=orentation;
    }

    public  void OnDrawOver(Canvas c,RecyclerView parent,RecyclerView.State state){

        if (Orentation==VERTICAL_LIST){
            drawVertical(c,parent);
        }
        else{
            drawHorizontal(c,parent);

        }

    }


  public  void   drawVertical(Canvas c,RecyclerView parent){
        final  int left=parent.getPaddingLeft();
        final  int right=parent.getWidth()- parent.getPaddingRight();
         final int childcount=parent.getChildCount();
         for (int i=0; i<childcount; i++){

        final View child = parent.getChildAt(i);
         final  RecyclerView.LayoutParams params = (RecyclerView.LayoutParams)child.getLayoutParams();

         final  int top=child.getBottom()+params.bottomMargin;
         final  int bottom=top+mDivider.getIntrinsicHeight();
             mDivider.setBounds(left + dpToPx(margien), top, right - dpToPx(margien), bottom);
             mDivider.draw(c);


         }





    }
    public  void   drawHorizontal(Canvas c,RecyclerView parent){

        final int top=parent.getPaddingTop();
        final  int bottom=parent.getWidth()-parent.getPaddingRight();
        final int childcount=parent.getChildCount();

        for (int i=0;i<childcount;i++) {
            final View child = parent.getChildAt(i);
            final RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child.getLayoutParams();
            final int left = child.getRight() + params.rightMargin;
            final int right = left + mDivider.getIntrinsicHeight();
            mDivider.setBounds(left, top + dpToPx(margien), right, bottom - dpToPx(margien));
            mDivider.draw(c);


        }



    }

    public void getItemOffsets(Rect outRect,View view,RecyclerView parent,RecyclerView.State state){
        if (Orentation==VERTICAL_LIST){
            outRect.set(0, 0, 0, mDivider.getIntrinsicHeight());


        }
        else {
            outRect.set(0, 0, mDivider.getIntrinsicWidth(), 0);
        }



    }
    private int dpToPx(int dp) {
        Resources r = context.getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }

}
