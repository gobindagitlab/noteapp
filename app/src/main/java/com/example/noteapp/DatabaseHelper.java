package com.example.noteapp;

public class DatabaseHelper extends android.database.sqlite.SQLiteOpenHelper {
    public static final String DATABASE_NAME    = "notes.db";
    public static final int    DATABASE_VERSION = 1;


    public
    DatabaseHelper ( @androidx.annotation.Nullable android.content.Context context ) {
        super ( context , DATABASE_NAME , null , DATABASE_VERSION );
    }

    @Override
    public
    void onCreate ( android.database.sqlite.SQLiteDatabase db ) {
        db.execSQL ( Note.CRATE_TABLE );

    }

    @Override
    public
    void onUpgrade ( android.database.sqlite.SQLiteDatabase db , int oldVersion , int newVersion ) {
        db.execSQL ( "DROP TABLE IFEXISTS" + com.example.noteapp.Note.TABLE_NAME );
        onCreate ( db );

    }


    public
    long InsetNote ( String note ) {
        android.database.sqlite.SQLiteDatabase db     = this.getWritableDatabase ( );
        android.content.ContentValues          values = new android.content.ContentValues ( );
        values.put ( com.example.noteapp.Note.COULUM_NOTE , note );
        long id = db.insert ( com.example.noteapp.Note.TABLE_NAME , null , values );
        db.close ( );
        return id;
    }

    public
    com.example.noteapp.Note getNote ( int id ) {
        android.database.sqlite.SQLiteDatabase db = this.getReadableDatabase ( );

        android.database.Cursor cursor = db.query ( com.example.noteapp.Note.TABLE_NAME , new String[] { com.example.noteapp.Note.COULUM_ID , com.example.noteapp.Note.COULUM_NOTE , com.example.noteapp.Note.COULUM_TIMESTAMP } , com.example.noteapp.Note.COULUM_ID + "=?" , new String[] { String.valueOf ( id ) } , null , null , null , null );
        if ( cursor == null )
            cursor.moveToFirst ( );
        Note note = new Note (
                cursor.getInt ( cursor.getColumnIndex ( com.example.noteapp.Note.COULUM_ID ) ) ,
                cursor.getString ( cursor.getColumnIndex ( com.example.noteapp.Note.COULUM_NOTE ) ) ,
                cursor.getString ( cursor.getColumnIndex ( com.example.noteapp.Note.COULUM_TIMESTAMP ) )

        );

        return note;


    }
   public
    java.util.List< com.example.noteapp.Note> getALLNotes(){

       java.util.List< com.example.noteapp.Note> notes=new java.util.ArrayList <> (  );

      String selectQuary= "SELECT  * FROM " + com.example.noteapp.Note.TABLE_NAME + "ORDER BY" + com.example.noteapp.Note.COULUM_TIMESTAMP+"DEC";


       android.database.sqlite.SQLiteDatabase db=this.getWritableDatabase ();

       android.database.Cursor cursor = db.rawQuery ( selectQuary, null );

       if ( cursor.moveToFirst () ){
           do {


               Note note=new Note (  );
                note.setId ( cursor.getInt ( cursor.getColumnIndex ( com.example.noteapp.Note.COULUM_ID ) ) );
                note.setNote ( cursor.getString ( cursor.getColumnIndex ( com.example.noteapp.Note.COULUM_NOTE ) ) );
                note.setTimetamp ( cursor.getString ( cursor.getColumnIndex ( com.example.noteapp.Note.COULUM_TIMESTAMP ) ) );

           }while ( cursor.moveToFirst () );

       }
       db.close ();

        return   notes;
   }


public  int getAllNotesCount(){
         String countQuary= "SELECT * FROM " + com.example.noteapp.Note.TABLE_NAME;

    android.database.sqlite.SQLiteDatabase db     =this.getReadableDatabase ();
    android.database.Cursor  cursor = db.rawQuery (countQuary, null );
      int count= cursor.getCount ();
      cursor.close ();
   return  count;
}

 public  int UpdateNotes(Note note){

     android.database.sqlite.SQLiteDatabase db=this.getWritableDatabase ();
     android.content.ContentValues values=new android.content.ContentValues (  );

     values.put ( com.example.noteapp.Note.COULUM_NOTE,note.getNote () );


     return db.update( Note.TABLE_NAME, values, com.example.noteapp.Note.COULUM_ID + " = ?",
                       new String[]{String.valueOf(note.getId())});
 }


 public  void DelateNotes(Note note){

     android.database.sqlite.SQLiteDatabase database=this.getWritableDatabase ();

 database.delete ( com.example.noteapp.Note.TABLE_NAME, com.example.noteapp.Note.COULUM_ID+"=?",new String[]{String.valueOf ( note.getId () )} );
 database.close ();


 }




}
