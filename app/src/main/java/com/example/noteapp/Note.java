package com.example.noteapp;

public class Note {
    public static  final String TABLE_NAME="notes";
     public  static  final  String COULUM_ID="id";
     public  static  final  String COULUM_NOTE="note";
     public  static  final  String COULUM_TIMESTAMP="timestamp";


     private  int id;
     private  String note;
     private  String timetamp;


public static final String  CRATE_TABLE="CREATETABLE"+TABLE_NAME+"("+COULUM_ID+" INTEGER PRIMARY KEY AUTOINCREMENT"+COULUM_NOTE+"TEXT"+COULUM_TIMESTAMP+" DATETIME DEFAULT CURRENT_TIMESTAMP"+")";

 public
 Note ( int id , String note , String timetamp ) {
  this.id       = id;
  this.note     = note;
  this.timetamp = timetamp;
 }

 public
 Note ( ) {
 }

 public
 int getId ( ) {
  return id;
 }

 public
 void setId ( int id ) {
  this.id = id;
 }

 public
 String getNote ( ) {
  return note;
 }

 public
 void setNote ( String note ) {
  this.note = note;
 }

 public
 String getTimetamp ( ) {
  return timetamp;
 }

 public
 void setTimetamp ( String timetamp ) {
  this.timetamp = timetamp;
 }
}
