package com.example.noteapp;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class NotesAdapter extends RecyclerView.Adapter<NotesAdapter.MyViewHolder> {
    private Context context;
    private List<Note> notesList;


    public NotesAdapter(Context context,List<Note> notesList) {
        this.context=context;
        this.notesList=notesList;

    }

    public  class MyViewHolder extends  RecyclerView.ViewHolder {
        public  TextView  note;
        public  TextView  dot;
        public  TextView  timesTamp;
        private android.widget.RelativeLayout relativeLayout;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            note=itemView.findViewById(R.id.note);
            dot=itemView.findViewById(R.id.dot);
             timesTamp=itemView.findViewById(R.id.timestamp);
            ;
        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
      View iteamview= LayoutInflater.from(parent.getContext()).inflate(R.layout.note_list_row,parent,false);
        return new MyViewHolder(iteamview);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Note note=notesList.get(position);

        holder.note.setText(note.getNote());
        holder.dot.setText(Html.fromHtml("&#8226;"));
        holder.timesTamp.setText(formatDate(note.getTimetamp()));

    }

    @Override
    public int getItemCount() {
        return notesList.size();
    }
    private String formatDate(String dateStr) {
        try {
            SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = fmt.parse(dateStr);
            SimpleDateFormat fmtOut = new SimpleDateFormat("MMM d");
            return fmtOut.format(date);
        } catch (ParseException e) {

        }

 return  "";
    }

}
