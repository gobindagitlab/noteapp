package com.example.noteapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Toolbar;

public class MainActivity extends AppCompatActivity {
    private com.example.noteapp.NotesAdapter  madapter;
    private java.util.List< com.example.noteapp.Note> noteslist=new java.util.ArrayList <> (  );
    private androidx.constraintlayout.widget.ConstraintLayout constraintLayout;
    private androidx.recyclerview.widget.RecyclerView recyclerView;
    private android.widget.TextView noNotesView;
    private  com.example.noteapp.DatabaseHelper db;
private android.widget.Toolbar toolbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
     toolbar=findViewById ( com.example.noteapp.R.id.toolbar );
     setSupportActionBar (toolbar);
     constraintLayout=findViewById ( com.example.noteapp.R.id.coordinator_layout );
     recyclerView=findViewById ( com.example.noteapp.R.id.recycler_view );
     noNotesView=findViewById ( com.example.noteapp.R.id.empty_notes_view );
     db=new DatabaseHelper ( this );
     noteslist.addAll ( db.getALLNotes () );

     com.google.android.material.floatingactionbutton.FloatingActionButton fav=findViewById ( com.example.noteapp.R.id.fab );

      fav.setOnClickListener ( new android.view.View.OnClickListener ( ) {
          @Override
          public
          void onClick ( android.view.View v ) {
              showNoteDialog(false, null, -1);

          }
      } );
      madapter=new NotesAdapter ( this,noteslist );
        androidx.recyclerview.widget.RecyclerView.LayoutManager mlayoutManager= new androidx.recyclerview.widget.LinearLayoutManager ( getApplicationContext () );
    recyclerView.setLayoutManager ( mlayoutManager );
     recyclerView.setItemAnimator ( new androidx.recyclerview.widget.DefaultItemAnimator () );
     recyclerView.addItemDecoration ( new MyDividerItemDecoratio (this, androidx.recyclerview.widget.LinearLayoutManager.VERTICAL,16 ) );
     recyclerView.setAdapter ( madapter );
     toggleEmptyNotes ();





    }


   private void  showNoteDialog( final  boolean ShouldUpdate, final com.example.noteapp.Note note,final int postion ){

       android.view.LayoutInflater layoutInflater = android.view.LayoutInflater.from ( getApplicationContext () );
           android.view.View  view   =layoutInflater.inflate ( com.example.noteapp.R.layout.note_dialoge, null );

       androidx.appcompat.app.AlertDialog.Builder  alaartdialogerbuilder=new androidx.appcompat.app.AlertDialog.Builder ( MainActivity.this );
       alaartdialogerbuilder.setView ( view );

       final android.widget.EditText inputnote=findViewById ( com.example.noteapp.R.id.note );
       android.widget.TextView dialogetitle=findViewById ( com.example.noteapp.R.id.dialog_title );

     dialogetitle.setText ( !ShouldUpdate?getString ( com.example.noteapp.R.string.lbl_new_note_title ):getString ( com.example.noteapp.R.string.lbl_edit_note_title ));
       if (ShouldUpdate && note != null) {
           inputnote.setText(note.getNote());
       }

       alaartdialogerbuilder.setCancelable ( false )
                            .setPositiveButton ( ShouldUpdate ? "Update" : "Save" , new android.content.DialogInterface.OnClickListener ( ) {
                                @Override
                                public
                                void onClick ( android.content.DialogInterface dialog ,
                                               int which ) {

                                }
                            } )
                            .setNegativeButton ( "Cancel" , new android.content.DialogInterface.OnClickListener ( ) {
                                @Override
                                public
                                void onClick ( android.content.DialogInterface dialog ,
                                               int which ) {
                                    dialog.cancel ();

                                }
                            } );

       final androidx.appcompat.app.AlertDialog alertDialog=alaartdialogerbuilder.create ();
            alertDialog.show ();



            alertDialog.getButton ( androidx.appcompat.app.AlertDialog.BUTTON_POSITIVE ).setOnClickListener ( new android.view.View.OnClickListener ( ) {
                @Override
                public
                void onClick ( android.view.View v ) {
                    if ( android.text.TextUtils.isEmpty ( inputnote.getText ().toString () ) ){
                        android.widget.Toast.makeText ( MainActivity.this, "Enter note!", android.widget.Toast.LENGTH_SHORT ).show ( );
                        return;

                    }
                    else{
                        alertDialog.dismiss();
                    }
                    if ( ShouldUpdate && note != null ) {
                        updateNote(inputnote.getText ().toString (),postion);

                    }
                    else{
                        createNote(inputnote.getText ().toString ());
                    }


                }
            } );


   }

   private  void updateNote(String note,int postion){
    Note n=noteslist.get ( postion );
    n.setNote ( note );
    db.UpdateNotes ( n );
    noteslist.set ( postion,n );
    madapter.notifyItemChanged ( postion );
       toggleEmptyNotes();

    }

    private void deleteNote(int position) {
        // deleting the note from db
        db.DelateNotes (noteslist.get(position));

        // removing the note from the list
        noteslist.remove(position);
        madapter.notifyItemRemoved(position);

        toggleEmptyNotes();
    }
   private void  createNote(String note){
       long id = db.InsetNote (note);

       // get the newly inserted note from db
       Note n = db.getNote ( ( int ) id );
       if ( n != null ) {
           noteslist.add ( 0,n);
           madapter.notifyDataSetChanged();
           toggleEmptyNotes();

       }


   }
   private void  toggleEmptyNotes(){
  if ( db.getAllNotesCount ()>0 ){
noNotesView.setVisibility ( android.view.View.GONE );

  }
  else{
      noNotesView.setVisibility ( android.view.View.VISIBLE );
  }


   }


    private
    void setSupportActionBar ( android.widget.Toolbar toolbar ) {
    }
    private void showActionsDialog(final int position) {
        CharSequence colors[] = new CharSequence[] { "Edit" , "Delete" };

        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder ( this );
        builder.setTitle ( "Choose option" );
        builder.setItems ( colors , new android.content.DialogInterface.OnClickListener ( ) {


            @Override
            public
            void onClick ( android.content.DialogInterface dialog , int which ) {
                if ( which == 0 ) {
                    showNoteDialog ( true , noteslist.get ( position ) , position );
                }
                else {
                    deleteNote ( position );
                }
            }
        } );
        builder.show ( );
    }


}
