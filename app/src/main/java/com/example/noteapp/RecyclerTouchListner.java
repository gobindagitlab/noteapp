package com.example.noteapp;

public class RecyclerTouchListner implements androidx.recyclerview.widget.RecyclerView.OnItemTouchListener {

    private ClickListener                clicklistener;
    private android.view.GestureDetector gestureDetector;

    public RecyclerTouchListner ( android.content.Context context, final androidx.recyclerview.widget.RecyclerView recycleView, final ClickListener clicklistener ) {

        this.clicklistener = clicklistener;
        gestureDetector = new android.view.GestureDetector ( context, new android.view.GestureDetector.SimpleOnGestureListener() {
            @Override
            public boolean onSingleTapUp ( android.view.MotionEvent e ) {
                return true;
            }

            @Override
            public void onLongPress ( android.view.MotionEvent e ) {
                android.view.View child = recycleView.findChildViewUnder ( e.getX ( ), e.getY ( ) );
                if (child != null && clicklistener != null) {
                    clicklistener.onLongClick(child, recycleView.getChildAdapterPosition(child));
                }
            }
        });
    }

    @Override
    public
    boolean onInterceptTouchEvent ( @androidx.annotation.NonNull androidx.recyclerview.widget.RecyclerView rv , @androidx.annotation.NonNull android.view.MotionEvent e ) {
        android.view.View child = rv.findChildViewUnder ( e.getX ( ), e.getY ( ) );
        if (child != null && clicklistener != null && gestureDetector.onTouchEvent(e)) {
            clicklistener.onClick(child, rv.getChildAdapterPosition(child));
        }

        return false;
    }

    @Override
    public
    void onTouchEvent ( @androidx.annotation.NonNull androidx.recyclerview.widget.RecyclerView rv , @androidx.annotation.NonNull android.view.MotionEvent e ) {

    }


    @Override
    public
    void onRequestDisallowInterceptTouchEvent ( boolean disallowIntercept ) {

    }

    public interface ClickListener {
        void onClick ( android.view.View view, int position );

        void onLongClick ( android.view.View view, int position );
    }
}
